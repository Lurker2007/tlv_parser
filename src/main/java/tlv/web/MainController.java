package tlv.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tlv.service.TagService;
import tlv.service.dto.Tlv;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    @Autowired private TagService tagService;

    String tlvAfterString;
    String tlvBeforeString = "";

    @RequestMapping("/")
    public String main(Model model) {
        if(tlvAfterString == null) {
            tlvAfterString = "";
        }
        model.addAttribute("tlv", tlvAfterString);
        model.addAttribute("tlvString", tlvBeforeString);
        return "index";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String convert(@ModelAttribute("tlvString") String tlvString) {
        this.tlvBeforeString = tlvString;
        this.tlvAfterString = tagService.decodingTlv(tlvString);
        return "redirect:" + "/";
    }


}
