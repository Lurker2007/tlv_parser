package tlv.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tlv.db.domain.Tag;

public interface TagRepository extends JpaRepository<Tag, Integer> {
    Tag findByNumber(Integer number);
}
