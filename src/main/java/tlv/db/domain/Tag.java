package tlv.db.domain;

import lombok.Data;
import lombok.experimental.Accessors;
import tlv.common.TagType;

import javax.persistence.*;

@Data
@Accessors(chain = true)
@Entity
@Table(name = "tag")
public class Tag {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "number")
    private Integer number;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private TagType type;
}
