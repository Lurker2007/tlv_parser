package tlv.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import tlv.db.domain.Tag;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class TlvLocalDateTime implements Tlv {
    private Tag tag;
    private Integer length;
    private LocalDateTime value;

    public String toString() {
        return "(" + tag.getNumber() + ") " + tag.getName() + ": " + value + "\n";
    }
}
