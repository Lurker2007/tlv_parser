package tlv.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import tlv.db.domain.Tag;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class TlvDigDecimal implements Tlv {
    private Tag tag;
    private Integer length;
    private BigDecimal value;

    public String toString() {
        return "(" + tag.getNumber() + ") " + tag.getName() + ": " + value + "\n";
    }
}
