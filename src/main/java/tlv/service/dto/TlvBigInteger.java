package tlv.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import tlv.db.domain.Tag;

import java.math.BigInteger;

@Data
@Accessors(chain = true)
public class TlvBigInteger implements Tlv {
    private Tag tag;
    private Integer length;
    private BigInteger value;

    public String toString() {
        return "(" + tag.getNumber() + ") " + tag.getName() + ": " + value + "\n";
    }
}
