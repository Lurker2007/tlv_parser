package tlv.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import tlv.common.TagType;
import tlv.db.domain.Tag;

public interface Tlv {
    String toString();
}
