package tlv.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import tlv.db.domain.Tag;

@Data
@Accessors(chain = true)
public class TlvInteger implements Tlv {
    private Tag tag;
    private Integer length;
    private Integer value;

    public String toString() {
        return "(" + tag.getNumber() + ") " + tag.getName() + ": " + value + "\n";
    }
}
