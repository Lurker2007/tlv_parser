package tlv.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import tlv.db.domain.Tag;

@Data
@Accessors(chain = true)
public class TlvString implements Tlv {
    private Tag tag;
    private Integer length;
    private String value;

    public String toString() {
        return "(" + tag.getNumber() + ") " + tag.getName() + ": " + value + "\n";
    }
}
