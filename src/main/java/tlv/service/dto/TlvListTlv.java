package tlv.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import tlv.db.domain.Tag;

import java.util.List;

@Data
@Accessors(chain = true)
public class TlvListTlv implements Tlv {
    private Tag tag;
    private Integer length;
    private List<Tlv> value;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(" + tag.getNumber() + ") " + tag.getName() + ": " + "\n");
        for(Tlv tlv: value) {
            sb.append("\t" + tlv.toString());
        }
        return sb.toString();
    }
}
