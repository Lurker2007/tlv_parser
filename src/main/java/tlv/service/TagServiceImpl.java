package tlv.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tlv.db.domain.Tag;
import tlv.db.repository.TagRepository;
import tlv.service.dto.*;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TagServiceImpl implements TagService {
    @Autowired TagRepository tagRepository;
    @Autowired ByteOperationService byteOperationService;

    @Override
    public String decodingTlv(String s) {
        String stringBytes = s.trim().replaceAll("[^a-fA-F0-9]+", "");
        if (stringBytes.length() % 2 != 0) {
            return null;
        }
        Byte[] bytes = hexStringToByteArray(stringBytes);
        return listTlvToString(byteArrayToTlv(bytes));
    }

    private String listTlvToString(List<Tlv> tlvs) {
        StringBuilder sb = new StringBuilder();
        for(Tlv tlv : tlvs) {
            sb.append(tlv.toString());
        }
        return sb.toString();
    }

    private Byte[] hexStringToByteArray(String s) {
        int len = s.length();
        Byte[] data = new Byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private List<Tlv> byteArrayToTlv(Byte[] bytes) {
        List<Tlv> result = new ArrayList<>();

        int counter = 0;
        List<Byte> tagBytes = new ArrayList<>();
        List<Byte> lengthBytes = new ArrayList<>();
        List<Byte> valueBytes = new ArrayList<>();
        Tlv tlv;

        int tagNumber = 0;
        int length = 0;

        for (int i = 0; i < bytes.length; i++) {
            if (counter < 2) {
                tagBytes.add(bytes[i]);
                if (tagBytes.size() == 2) {
                    tagNumber = byteOperationService.getIntFromByte(tagBytes);
                }
            } else if (counter >= 2 && counter < 4) {
                lengthBytes.add(bytes[i]);
                if (lengthBytes.size() == 2) {
                    length = byteOperationService.getIntFromByte(lengthBytes);
                }
            } else {
                valueBytes.add(bytes[i]);
                if (valueBytes.size() == length) {
                    Tag tag = tagRepository.findByNumber(tagNumber);
                    switch (tag.getType()) {
                        case UINT16:
                            tlv = new TlvInteger()
                                    .setTag(tag)
                                    .setLength(length)
                                    .setValue(byteOperationService.getIntFromByte(valueBytes));
                            break;
                        case BYTE:
                            valueBytes.add((byte) 0);
                            tlv = new TlvInteger()
                                    .setTag(tag)
                                    .setLength(length)
                                    .setValue(byteOperationService.getIntFromByte(valueBytes));
                            break;
                        case STRING:
                            tlv = new TlvString()
                                    .setTag(tag)
                                    .setLength(length)
                                    .setValue(byteOperationService.getStringFromByte(valueBytes));
                            break;
                        case FVLN:
                            tlv = new TlvDigDecimal()
                                    .setTag(tag)
                                    .setLength(length)
                                    .setValue(byteOperationService.getBigDecimalFromByte(valueBytes));
                            break;
                        case UINT32:
                            tlv = new TlvBigInteger()
                                    .setTag(tag)
                                    .setLength(length)
                                    .setValue(byteOperationService.getBigIntegerFromByte(valueBytes, ByteOrder.LITTLE_ENDIAN));
                            break;
                        case UNIXTIME:
                            tlv = new TlvLocalDateTime()
                                    .setTag(tag)
                                    .setLength(length)
                                    .setValue(byteOperationService.getLocalDateFromByte(valueBytes));
                            break;
                        case REGISTER:
                            tlv = new TlvInteger()
                                    .setTag(tag)
                                    .setLength(length)
                                    .setValue(byteOperationService.getBitOrderFromByte(valueBytes));
                            break;
                        case BYTEARRAY:
                            tlv = new TlvBigInteger()
                                    .setTag(tag)
                                    .setLength(length)
                                    .setValue(byteOperationService.getBigIntegerFromByte(valueBytes.subList(2, valueBytes.size()), ByteOrder.BIG_ENDIAN));
                            break;
                        case VLN:
                            tlv = new TlvDigDecimal()
                                    .setTag(tag)
                                    .setLength(length)
                                    .setValue(bigIntegerToBigDecimalFln(byteOperationService.getBigIntegerFromByte(valueBytes, ByteOrder.LITTLE_ENDIAN)));
                            break;
                        default:
                            tlv = new TlvListTlv()
                                    .setTag(tag)
                                    .setLength(length)
                                    .setValue(byteArrayToTlv(valueBytes.toArray(new Byte[valueBytes.size()])));
                            break;
                    }
                    result.add(tlv);
                    tlv = null;
                    counter = 0;
                    length = 0;
                    tagBytes = new ArrayList<>();
                    lengthBytes = new ArrayList<>();
                    valueBytes = new ArrayList<>();
                    continue;
                }
            }
            counter++;
        }
        return result;
    }

    private BigDecimal bigIntegerToBigDecimalFln(BigInteger integer) {
        BigDecimal bigDecimal = new BigDecimal(integer);
        return bigDecimal.divide(BigDecimal.valueOf(100));
    }

}
