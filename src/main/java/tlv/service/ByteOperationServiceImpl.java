package tlv.service;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Service
public class ByteOperationServiceImpl implements ByteOperationService {

    @Override
    public Integer getIntFromByte(List<Byte> bytes) {
        byte[] array = ArrayUtils.toPrimitive(bytes.toArray(new Byte[bytes.size()]));
        ByteBuffer wrap = ByteBuffer.wrap(array).order(ByteOrder.LITTLE_ENDIAN);
        return new Integer(wrap.getShort());
    }

    @Override
    public String getStringFromByte(List<Byte> bytes) {
        byte[] array = ArrayUtils.toPrimitive(bytes.toArray(new Byte[bytes.size()]));
        String str = new String(array, Charset.forName("cp866"));
        return str;
    }

    @Override
    public BigInteger getBigIntegerFromByte(List<Byte> bytes, ByteOrder order) {
        byte[] array = ArrayUtils.toPrimitive(bytes.toArray(new Byte[bytes.size()]));
        if (order.equals(ByteOrder.LITTLE_ENDIAN)) {
            ArrayUtils.reverse(array);
        }
        return new BigInteger(array);
    }

    @Override
    public Integer getBitOrderFromByte(List<Byte> bytes) {
        byte[] array = ArrayUtils.toPrimitive(bytes.toArray(new Byte[bytes.size()]));
        int value = new BigInteger(1, array).intValue();
        return Integer.highestOneBit(value) - 1;
    }

    @Override
    public BigDecimal getBigDecimalFromByte(List<Byte> bytes) {
        byte[] array = ArrayUtils.toPrimitive(bytes.toArray(new Byte[bytes.size()]));
        int floatPoint = array[0];
        byte[] arrayAfterRemove = ArrayUtils.remove(array, 0);
        ArrayUtils.reverse(arrayAfterRemove);
        BigDecimal bigDecimal = new BigDecimal(new BigInteger(1, arrayAfterRemove));
        return bigDecimal.divide(BigDecimal.valueOf((int) Math.pow(10, floatPoint)));
    }

    @Override
    public LocalDateTime getLocalDateFromByte(List<Byte> bytes) {
        byte[] array = ArrayUtils.toPrimitive(bytes.toArray(new Byte[bytes.size()]));
        ArrayUtils.reverse(array);
        BigInteger timestamp = new BigInteger(1, array);
        LocalDateTime dateTime = Instant.ofEpochSecond(timestamp.longValue()).atZone(ZoneId.systemDefault()).toLocalDateTime();
        return dateTime;
    }


}
