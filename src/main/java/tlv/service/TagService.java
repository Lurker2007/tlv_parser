package tlv.service;

import tlv.db.domain.Tag;
import tlv.service.dto.Tlv;

import java.util.List;

public interface TagService {
    String decodingTlv(String s);
}
