package tlv.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by alexey on 25.12.16.
 */
public interface ByteOperationService {
    Integer getIntFromByte(List<Byte> bytes);
    String getStringFromByte(List<Byte> bytes);
    BigDecimal getBigDecimalFromByte(List<Byte> bytes);
    LocalDateTime getLocalDateFromByte(List<Byte> bytes);
    BigInteger getBigIntegerFromByte(List<Byte> bytes, ByteOrder order);
    Integer getBitOrderFromByte(List<Byte> bytes);
}
