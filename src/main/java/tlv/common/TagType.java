package tlv.common;

/**
 * Created by alexey on 25.12.16.
 */
public enum TagType {
    STLV,
    BYTE,
    UINT16,
    UINT32,
    REGISTER,
    VLN,
    FVLN,
    UNIXTIME,
    STRING,
    BYTEARRAY
}
